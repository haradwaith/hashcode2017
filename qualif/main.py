#!/usr/bin/env python
from __future__ import print_function

import sys
import math

from cache import Cache
from video import Video


def put(msg):
    print(msg, file=sys.stderr)


def parse_line(f):
    return [int(value) for value in f.readline().split()]


def parse(input_file):
    with open(input_file) as f:
        V, E, R, C, X = parse_line(f)
        caches = [Cache(i, X) for i in range(C)]
        videos = [Video(i, lat) for i, lat in enumerate(parse_line(f))]
        endpoints = []
        for _ in range(E):
            endpoint = {"caches": []}
            endpoint['Ld'], endpoint['K'] = parse_line(f)
            endpoint['caches'] = {}
            for i in range(endpoint['K']):
                cache_id, Lc = parse_line(f)
                endpoint['caches'][caches[cache_id]] = Lc
            endpoint['sorted_caches'] = sorted(endpoint["caches"], key=endpoint["caches"].__getitem__)
            endpoints.append(endpoint)
        requests = [parse_line(f) for _ in range(R)]
    return V, E, R, C, X, videos, endpoints, requests, caches


def format_output(caches):
    print(len(caches))
    for cache in caches:
        print(cache)


def score(videos, endpoints, requests, caches):
    score = 0
    i = 0
    for req in requests:
        i += 1
        if i % 10000 == 0:
            put(i)
        endpoint = endpoints[req[1]]
        video = videos[req[0]]
        for cache in endpoint['sorted_caches']:
            if video.is_in(cache):
                score += req[2] * (endpoint["Ld"] - endpoint["caches"][cache])
                break
    final_score = score * 1000.0 / sum(req[2] for req in requests)
    put("Score: {}".format(final_score))
    return final_score


def find_best_cache(video, caches):
    for cache in caches:
        if video.is_in(cache):
            break
        if cache.available > video.size:
            cache.store(video)
            break


def bidon(videos, endpoints, requests, caches):
    sorted_requests = [t[1] for t in sorted([(compute_max_score(r, endpoints, videos), r) for r in requests], key=lambda m: -m[0])]
    for req in sorted_requests:
        endpoint = endpoints[req[1]]
        video = videos[req[0]]
        if not any(video.is_in(cache) for cache in endpoint['sorted_caches']):
            find_best_cache(video, endpoint['sorted_caches'])
    for req in sorted_requests:
        endpoint = endpoints[req[1]]
        video = videos[req[0]]
        find_best_cache(video, endpoint['sorted_caches'])


def compute_max_score(request, endpoints, videos):
    endpoint = endpoints[request[1]]
    number = request[2]
    ld = endpoint['Ld']
    if len(endpoint['caches']) > 0:
        best = min([v for k, v in endpoint['caches'].items()])
    else:
        best = 0
    return number * (ld - best) / math.log(1 + videos[request[0]].size)


def videos_by_requests(videos, endpoints, requests, caches):
    result = {video.id: {'requests': [], 'count': 0} for video in videos}
    for req in requests:
        result[req[0]]['requests'] += [req]
        result[req[0]]['count'] += req[2] / ((videos[req[0]].size)**0.9)
    result = sorted(result.values(), key=lambda x: -x['count'])
    for row in result:
        by_cache = {cache.id: 0 for cache in caches}
        for req in row['requests']:
            req_latency = endpoints[req[1]]['Ld']
            for cache in endpoints[req[1]]['sorted_caches']:
                latency = endpoints[req[1]]['caches'][cache]
                by_cache[cache.id] += req[2] * (req_latency - latency)
        video = videos[req[0]]
        sorted_caches = [caches[k] for k in reversed(sorted(by_cache, key=by_cache.__getitem__))]
        for cache in sorted_caches:
            if video.is_in(cache):
                continue
            if cache.available > video.size:
                cache.store(video)
                break


def process(input_file):
    V, E, R, C, X, videos, endpoints, requests, caches = parse(input_file)
    videos_by_requests(videos, endpoints, requests, caches)
    bidon(videos, endpoints, requests, caches)
    format_output(caches)
    #if "kitten" not in input_file:
    score(videos, endpoints, requests, caches)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("usage: python main.py <dataset.in>")
        sys.exit(1)
    input_file = sys.argv[1]
    process(input_file)
