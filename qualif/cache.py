class Cache():
    def __init__(self, i, X):
        self.id = i
        self.X = X
        self.available = X
        self.content = set()
        self.content_id = set()

    def store(self, video):
        assert video.id not in self.content_id, "video already cached"
        assert self.available >= video.size, "no space left on cache"
        self.content.add(video)
        self.content_id.add(video.id)
        self.available -= video.size
        video.add_to_cache(self)

    def contains(self, video):
        return video.id in self.content_id

    def __str__(self):
        return " ".join([str(self.id)] + [str(x) for x in self.content])

    def __repr__(self):
        return "Cache {} ({} videos stored)".format(self.id, len(self.content))
