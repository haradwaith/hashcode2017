class Video():
    def __init__(self, i, size):
        self.id = i
        self.size = size
        self.caches = set()
        self.caches_id = set()

    def add_to_cache(self, cache):
        assert cache.id not in self.caches_id, "cache already used"
        self.caches.add(cache)
        self.caches_id.add(cache.id)

    def is_in(self, cache):
        return cache.id in self.caches_id

    def __str__(self):
        return str(self.id)

    def __repr__(self):
        return "Video {}".format(self.id)
