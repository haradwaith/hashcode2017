from pos import Pos
class Car():
    def __init__(self, idx):
        self.idx = idx
        self.rides = []
        self.failed = 0
        self.time_finished = 0
        self.pos_finished = Pos(0, 0)

    #Returns a tuple, first element is true/false based on whether this is possible
    #Second, if we succeed, is how many supplemental rides will miss the deadline
    def can_perform_ride(self, ride):
        newRides = sorted(self.rides + [ride], key = lambda r: r.earliest)
        #How much are we late to earliest
        delta = 0
        pos = Pos(0, 0)
        curTime = 0
        failed = 0
        for myRide in newRides:
            if curTime + pos.dst(myRide.start) + myRide.duration() > myRide.latest:
                return (False, 0)
            curTime = max(curTime + pos.dst(myRide.start), myRide.earliest)
            if curTime > myRide.earliest:
                failed = 1
            curTime += myRide.duration()
            pos = myRide.end
        return (True, failed - self.failed)

    def earliest_time(self, ride):
        previousRide = None
        delta = 0
        pos = Pos(0, 0)
        curTime = 0
        for myRide in self.rides:
            if myRide.earliest > ride.earliest:
                break

            if curTime + pos.dst(myRide.start) + myRide.duration() > myRide.latest:
                return False
            curTime = max(curTime + pos.dst(myRide.start), myRide.earliest)
            curTime += myRide.duration()
            pos = myRide.end

        return pos.dst(ride.start)

    def earliest_time(self, ride):
        previousRide = None
        delta = 0
        pos = Pos(0, 0)
        curTime = 0
        for myRide in self.rides:
            if myRide.earliest > ride.earliest:
                break

            if curTime + pos.dst(myRide.start) + myRide.duration() > myRide.latest:
                return False
            curTime = max(curTime + pos.dst(myRide.start), myRide.earliest)
            curTime += myRide.duration()
            pos = myRide.end

        return pos.dst(ride.start)

    def add_ride(self, ride):
        ride.car = self
        self.rides.append(ride)
        r = self.can_perform_ride(ride)
        self.failed += r[1]
        self.rides = sorted(self.rides, key=lambda r: r.earliest)

        self.time_finished = max(self.time_finished + self.pos_finished.dst(ride.start), ride.earliest) + ride.duration()
        self.pos_finished = ride.end

    def format_output(self):
        return " ".join([str(len(self.rides))] + [str(ride.idx) for ride in self.rides])

    def __str__(self):
        return "Car {} - {} rides".format(self.idx, len(self.rides))

    def __repr__(self):
        return "Car {}".format(self.idx)
