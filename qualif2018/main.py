#!/usr/bin/env python3
import random
import sys
import math

from ride import Ride
from pos import Pos
from car import Car


def put(msg):
    print(msg, file=sys.stderr)


def parse_line(f):
    return [int(value) for value in f.readline().split()]


def parse(input_file):
    with open(input_file) as f:
        R, C, F, N, B, T = parse_line(f)
        rides = [Ride(i, parse_line(f)) for i in range(N)]
        cars = [Car(i) for i in range(F)]
    return R, C, F, N, B, T, rides, cars


def find_best_rides(R, C, F, N, B, T, rides, cars):
    newRides = sorted(rides, key=lambda m: -m.start.dst(m.end))
    for ride in newRides:
        newCars = sorted(cars, key=lambda c: c.earliest_time(ride))
        for car in newCars:
            r = car.can_perform_ride(ride)
            if r[0] and r[1] <= 0:
                car.add_ride(ride)
                break

    for ride in newRides:
        if ride.car != None: continue
        for car in cars:
            r = car.can_perform_ride(ride)
            if r[0] and r[1] == 1:
                car.add_ride(ride)
                break


def find_best_rides_cards(R, C, F, N, B, T, rides, cars):
    start_idx = 0
    remaining_rides = list(rides)
    while start_idx < len(cars):
        newCars = sorted(cars, key=lambda c: c.time_finished)
        finished = False
        for idx in range(start_idx, len(newCars)):
            car = newCars[idx]
            newRides = sorted(remaining_rides, key=lambda r: max(car.time_finished + car.pos_finished.dst(r.start), r.earliest))
            for ride in newRides:
                if car.can_perform_ride(ride)[0]:
                    car.add_ride(ride)
                    remaining_rides.remove(ride)
                    finished = True
                    break
            if finished:
                break
            else:
                start_idx += 1

def format_output(cars):
    for car in cars:
        print(car.format_output())


def score(cars, B, T):
    s = 0
    for car in cars:
        pos = Pos(0, 0)
        t = 0
        for ride in car.rides:
            bonus = False
            t += ride.start.dst(pos)
            if t > T:
                break
            if t <= ride.earliest:
                t = ride.earliest
                bonus = True
            t += ride.end.dst(ride.start)
            pos = ride.end
            if t <= ride.latest and t < T:
                s += ride.duration()
                if bonus:
                    s += B
            else:
                put("Warning: this was a useless ride")
    return s

def process(input_file):
    R, C, F, N, B, T, rides, cars = parse(input_file)
    find_best_rides_cards(R, C, F, N, B, T, rides, cars)
    put("Score: {}".format(score(cars, B, T)))
    format_output(cars)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("usage: python3 main.py <dataset.in>")
        sys.exit(1)
    input_file = sys.argv[1]
    process(input_file)
