import math

class Pos():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "({}, {})".format(self.x, self.y)

    def __repr__(self):
        return self.__str__()

    def dst(self, other):
        return abs(self.x - other.x) + abs(self.y - other.y)
