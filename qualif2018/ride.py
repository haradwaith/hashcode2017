from pos import Pos

class Ride():
    def __init__(self, idx, data):
        self.idx = idx
        self.start = Pos(data[0], data[1])
        self.end = Pos(data[2], data[3])
        self.earliest = data[4]
        self.latest = data[5]
        self.car = None

    def duration(self):
        return self.end.dst(self.start)

    def __str__(self):
        return "Ride {}: {} to {} after {} before {}".format(self.idx, self.start, self.end, self.earliest, self.latest)

    def __repr__(self):
        return "Ride {}".format(self.idx)

    def dst(self, other):
        if self.earliest > other.earliest:
            return other.compatible(self)

        #from here, we know that self is before other
        return self.end.dst(other.start)

    def compatible(self, other):
        if self.earliest > other.earliest:
            return other.compatible(self)

        #from here, we know that self is before other
        return other.latest > (self.latest + self.dst(other) + other.duration())
