# HashCode 2017

## Judge System URL

https://hashcodejudge.withgoogle.com/#/home

## Run all datasets

To run the program on all input datasets:
```bash
make all
```
The output files will be placed in the `out` drectory.

To delete all output files:
```bash
make clean
```
