#!/usr/bin/env python

import sys


def parse(input_file):
    with open(input_file) as f:
        R, C, L, H = [int(value) for value in f.readline().split()]
        pizza = [list(line.strip()) for line in f.readlines()]
    return R, C, L, H, pizza


def format_output(slices):
    print len(slices)
    for s in slices:
        print " ".join(str(value) for value in s)


def search_slices(R, C, L, H, pizza):
    for r1 in range(R):
        for r2 in range(r1, R):
            for c1 in range(C):
                for c2 in range(c1, C):
                    slice_size = (r2 - r1 + 1) * (c2 - c1 + 1)
                    if slice_size > H:
                        continue
                    nb_mushrooms = sum(1 for x in range(r1, r2 + 1) for y in range(c1, c2 + 1) if pizza[x][y] == "M")
                    if nb_mushrooms < L:
                        continue
                    nb_tomatoes = sum(1 for x in range(r1, r2 + 1) for y in range(c1, c2 + 1) if pizza[x][y] == "T")
                    if nb_tomatoes < L:
                        continue
                    return [(r1, c1, r2, c2)]
    return []


def process(input_file):
    R, C, L, H, pizza = parse(input_file)
    slices = search_slices(R, C, L, H, pizza)
    format_output(slices)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "usage: python main.py <dataset.in>"
        sys.exit(1)
    input_file = sys.argv[1]
    process(input_file)
